section .text
 
 
; Принимает код возврата и завершает текущий процесс
exit: 
    xor rax, rax,
    ret 

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
    .loop:
	    cmp byte [rdi + rax], 0
	    je .end
	    inc rax
	    jmp .loop
    .end:		
        ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    push rdx
    xor rax, rax
    call string_length; получаем длину стоки
    mov rdx, rax; передаем длину строки
    mov rsi, rdi; передаем строку
    mov rax, 1; номер системного вызова
    mov rdi, 1; stdout
    syscall
    pop rdx
    ret

; Принимает код символа и выводит его в stdout
print_char:
    xor rax, rax
    push rdx
    push rdi; сохранение в стеке cимвола
    mov rax, 1; номер системного вызова
    mov rdi, 1; stdout
    mov rsi, rsp; передаем строку из начала стека
    mov rdx, 1; 1 символ
    syscall
    pop rdi
    pop rdx
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, 0xA ; записываем симвод 0xA в агрумент
    call print_char; вызов предыдущей функции с аргументом 0xA
    ret

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    push rsp
    push r8
    push r9
    push rdx
    xor rax, rax
    mov rax, rdi; передаем число в аккамулятор
    mov r8, 10; делитель
    mov r9, rsp; сохраняем rsp
    dec rsp
    .loop:
        xor rdx, rdx; чистим остаток
        div r8
        add dl, '0'; переводим в символ
        dec rsp, 
        mov [rsp], dl; сохраняем символ в стеке
        cmp rax, 0; проверяем на конец 
        jne .loop
    mov rdi, rsp; передаем в качестве аргумента начало строки (в нашем случаем числа)
    call print_string; печатаем число
    mov rsp, r9; востанавливает rsp
    pop rdx
    pop r9
    pop r8
    pop rsp
    ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    xor rax, rax
    cmp rdi, 0; сравниваем с 0
    jge .plus; если больше 0, то переходим к .plus
    push rdi; сохраняем rdi
    mov rdi, '-'; в аргумент записываем "-"
    call print_char; выводим "-"
    pop rdi; востанавливаем rdi
    neg rdi; делаем rdi положительным
    .plus:
	    call print_uint; печатаем  модуль числа 
        ret

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    push rcx
    xor rax, rax
    xor rcx, rcx
    .loop:
    mov cl, byte[rdi+rax]
	cmp byte[rsi+rax], cl; сравнение байтов
	jne .not_equels ; если не равны, то вывод 0
	cmp byte[rdi+rax], 0; проверка на конец 
	je  .equels; если конец, то вывод 1
	inc rax 
	jmp .loop
    .not_equels:
	mov rax, 0
    pop rcx
	ret	 
    .equels:
	mov rax, 1
    pop rcx
	ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    push rsp
    push rdx
    xor rax, rax ; отчищаем rax и сразу кладем туда 0, то есть номер системного возова чтения
    push rax
    mov rdi, 0 ; stdin	
    mov rsi, rsp; указазываем куда записывать
    mov rdx, 1; один символ
    syscall
    pop rax; достаем записанное значение
    pop rdx
    pop rsp
    ret 

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:

    push r10
    push r8
    push r9

    xor rax, rax
    xor rdx, rdx
    xor r10, r10
    mov r8, rdi
    mov r9, rsi
    .skip:
        call read_char; проперка первого символа
        cmp ax, 0 ; если это 0 то конец слова 
	    je .end
	    cmp ax, 0x20
	    je .skip
	    cmp ax, 0x9
	    je .skip
	    cmp ax, 0xA
	    je .skip
	.read:
	    dec r9 ; уменьшаем счетчик длины слова для проверки ошибки
	    cmp r9, 0
	    je .error
	    mov [r8 + r10], rax; сохраняем считанный символ
	    inc r10; увеличиваем счетчик счетчик считанных символов
	    call read_char; считываем новый символ
	    cmp al, 0 ; проверяем на конец
	    je .end
	    cmp al, 0x20
	    je .end
	    cmp al, 0xA
	    je .end
	    cmp al, 0x9
	    je .end
	    jmp .read
    .end:
        mov rdx, r10
	    mov rax, r8 ; если считалось все слово, то выводим в rax результат
        pop r9
        pop r8
        pop r10

	    ret
    .error:
	    mov rax, 0; если ошибка, то выводим 0 в rax
        pop r9
        pop r8
        pop r10

	    ret 
 

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    push r8
    xor rax, rax
    xor rdx, rdx
    xor r8, r8
    .loop:
	    cmp byte[rdi], '9'; цифра не может быть больше 9
	    ja .end
	    cmp byte[rdi], '0' ; цифра не может быть меньше 0 
	    jb .end
	    inc r8 ; счетчик прочитанных цифр увеличиваем на 1
        imul rax, 10 ; умножаем на 10, чтобы прибавить цифру
        add al, byte[rdi] ; прибавляем цифру
	    sub al, '0'; получаем число
	    inc rdi ; увечиливаем счетчик длины
	    jmp .loop
    .end:
	    mov rdx, r8 ; сохраняем результат
        pop r8
        ret
	




; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:

    xor rax, rax
    cmp byte[rdi], '-' ; проверяем знак 
    jne .plus; если больше 0 
    inc rdi ; переходим к следующей цифре
    call parse_uint ; пытаемся прочитать, начиная с этой цифры
    inc rdx ; т.к был знак увеличиваем сетчик на 1
	neg rax ; изменяем знак числа
    ret
    .plus: ; читаем модуль числа
        call parse_uint

        ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    push rcx
    push r9
    xor rax, rax
    xor rcx, rcx
    xor r9, r9
    .loop:
	    cmp rax, rdx ; сравнение длину строки с длиной буфера
	    je .error
        mov cl, byte[rdi + rax] ; 
	    mov byte[rsi + rax], cl ;копируем символ в буфер
	    cmp byte[rdi + rax], 0 ; проверяем на конец
	    je .end
	    inc rax ; увеличиваем длину строки
	    jmp .loop
    .error:
	    mov rax, 0
        pop r9
        pop rcx
	    ret
    .end:
        pop r9
        pop rcx
	    ret

